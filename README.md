# Honeywell RTH6580WF

The Honeywell RTH6580WF is a basic WiFi-enabled thermostat. It is an interesting target for tinkering because it runs on an Atmel (now Microchip) XMEGA128B1 AVR microcontroller.

Older versions of the Honeywell line of programmable thermostats (non-WiFi) which used other AVR microcontrollers have proven to be re-programmable with new firmware. Perhaps we can do the same with the RTH6580WF in order to have a WiFi-enabled thermostat that does not depend on a cloud service (and thus cannot collect data on you or risk leaking it to 3rd parties).
